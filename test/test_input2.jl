using LinearAlgebra, MPI, Optim, LineSearches, Pkg
using VMEC, StellaratorOptimizationMetrics
Pkg.activate("/home/bfaber/projects/julia/PlasmaTurbulenceSaturationModel/")
using PlasmaTurbulenceSaturationModel
Pkg.activate("/home/bfaber/projects/julia/StellaratorOptimization/")
using StellaratorOptimization

MPI.Init()
#BLAS.set_num_threads(Threads.nthreads())

comms = StellOpt.setup_comms(8, MPI.COMM_WORLD)

opt = StellOpt.Optimizer(ConjugateGradient(alphaguess=1e-4, linesearch=BackTracking()),
                         Optim.Options(iterations=100))
prob = StellOpt.Problem()
StellOpt.add_optimizer!(prob, opt)

nml = VMEC.read_vmec_namelist("/home/bfaber/projects/julia/StellaratorOptimization/input.aten")

vmec_wrapper = StellOpt.EquilibriumWrapper{Float64, Vmec}(nml)
#vmec = readVmecWout("/home/bfaber/data/equilibria/QHS46/wout_qhs46.nc")
#StellOpt.update_equilibrium!(vmec_wrapper, vmec)
StellOpt.add_equilibrium_wrapper!(prob, vmec_wrapper)

#vmec_s02 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.2, equilibrium = vmec_wrapper))
vmec_s05 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.5, equilibrium = vmec_wrapper))
#vmec_s08 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.8, equilibrium = vmec_wrapper))

StellOpt.@add_target!(prob, PTSM.growth_rates([0.4], geometry = vmec_s05, nky = 1, Δky = 0.4, nkx = 1, Δkx = 0.2, solnInterval = 16π, points_per_2pi = 64, prec = Float32), 0.0, 1.0, "itg(ky = 0.4)")

StellOpt.@add_target!(prob, PTSM.growth_rates([0.7], geometry = vmec_s05, nky = 1, Δky = 0.7, nkx = 1, Δkx = 0.2, solnInterval = 16π, points_per_2pi = 64, prec = Float32), 0.0, 1.0, "itg(ky = 0.7)")

#=
StellOpt.@add_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, geometry = vmec_s02), 0.0, 1.0, "qs_vmec(s=0.2)")
StellOpt.@add_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, geometry = vmec_s05), 0.0, 1.0, "qs_vmec(s=0.5)")
StellOpt.@add_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, geometry = vmec_s08), 0.0, 1.0, "qs_vmec(s=0.8)")

ζ_min = 0.0
ζ_max = 100π
Δζ = 2π/100
 
StellOpt.@add_target!(prob,
                   StellOptMetrics.GammaC.gamma_c_target(geometry = vmec_s02, ζ_min, Δζ, ζ_max,
                                                         100, false, true), 
                   0.0, 1.0, "Γc(s=0.2)")
StellOpt.@add_target!(prob,
                   StellOptMetrics.GammaC.gamma_c_target(geometry = vmec_s05, ζ_min, Δζ, ζ_max,
                                                         100, false, true), 
                   0.0, 1.0, "Γc(s=0.5)")
StellOpt.@add_target!(prob,
                   StellOptMetrics.GammaC.gamma_c_target(geometry = vmec_s08, ζ_min, Δζ, ζ_max,
                                                         100, false, true), 
                   0.0, 1.0, "Γc(s=0.8)")
=#
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(1,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(1,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(2,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(2,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(3,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(3,0)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-3,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-3,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-2,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-2,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-1,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-1,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(0,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(0,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(1,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(1,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(2,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(2,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(3,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(3,1)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-3,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-3,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-2,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-2,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-1,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-1,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(0,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(0,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(1,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(1,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(2,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(2,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(3,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(3,2)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-3,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-3,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-2,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-2,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(-1,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(-1,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(0,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(0,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(1,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(1,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(2,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(2,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "rbc(3,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)
StellOpt.add_variable!(prob, vmec_wrapper, "zbs(3,3)", finite_diff_step = 0.01, diff_type = :ForwardDiff)


residual = StellOpt.optimize_mpi(prob, comms)
