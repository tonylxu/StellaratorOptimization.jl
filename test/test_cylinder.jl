using Test
using MPI, LinearAlgebra, Optim, LineSearches, JLD2
using VMEC, PlasmaEquilibriumToolkit, StellaratorOptimization

function r_midplane(vmecsurf::VmecSurface)
  return inverseTransform(VmecCoordinates(0.0, 0.0, 0.0), vmecsurf.rmn)
end

MPI.Init()
BLAS.set_num_threads(Threads.nthreads())

MPI.Barrier(MPI.COMM_WORLD)
comms = StellOpt.setup_comms(1, MPI.COMM_WORLD)
opt = StellOpt.Optimizer(BFGS(alphaguess=1e-7, linesearch=HagerZhang()),
                           Optim.Options(iterations=4, f_tol = 1e-10, allow_f_increases = true))
prob = StellOpt.Problem()
prob.output_tag = "test_circular"
prob.output_directory = @__DIR__
StellOpt.add_optimizer!(prob, opt)

nml = VMEC.read_vmec_namelist(joinpath(@__DIR__, "input.circular_tokamak"))
vmec_wrapper = StellOpt.EquilibriumWrapper(nml, Vmec, StellOpt.ForwardDiff(1e-4))
StellOpt.add_equilibrium_wrapper!(prob, vmec_wrapper)

vmec_s3 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.3, equilibrium = vmec_wrapper))
vmec_s7 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.7, equilibrium = vmec_wrapper))

StellOpt.@add_target!(prob, r_midplane(vmec_s3), 7.21, 1.0, "r03")
StellOpt.@add_target!(prob, r_midplane(vmec_s7), 7.73, 1.0, "r07")

#this won't add the 0,0 mode
StellOpt.add_multiple_modes!(prob, vmec_wrapper, 0, 0, max_m=1)



@testset "Test loading scripts" begin

  @testset "test opt reading" begin
    @test opt.options.f_reltol == 1.0e-10
    @test opt.options.iterations == 4
    @test opt.method.alphaguess!.alpha == 1.0e-7
  end



  @testset "read input nml" begin
    @test nml[:nfp] == 1
    @test nml[:raxis] == 6.0
    @test nml[:zaxis] == 0.0
    @test nml[Symbol("rbc(0,0)")] == 6.0
    @test nml[Symbol("zbs(0,0)")] == 0.0
    @test nml[Symbol("rbc(0,1)")] == 2.0
    @test nml[Symbol("zbs(0,1)")] == 2.0
  end


  @testset "test problem setup" begin
    @test length(prob.state_vector) == 2
    @test length(prob.targets) == 2
    @test prob.optimizer == opt
  end
end

MPI.Barrier(comms.world_comm)
residual = StellOpt.optimize_mpi(prob, comms)
println(residual)
