#using Optim, PlasmaEquilibriumToolkit, NE3DLE, StellaratorOptimization, LineSearches
#ENV["JULIA_DEBUG"] = StellOpt

optimizer = StellOpt.Optimizer(BFGS(; alphaguess=1e-5, linesearch=HagerZhang()),
                               Optim.Options(iterations = 1, store_trace = true, extended_trace = true))
prob = StellOpt.Problem()
prob.output_tag = "test"
prob.output_directory = "/home/bfaber/data/opt/fd_test/ne3dle_tests"
StellOpt.add_optimizer!(prob, optimizer)

fd = StellOpt.ForwardDiff(1e-4)
#spsa = StellOpt.SPSA(δ = 1e-5)
#nml = NE3DLE.read_par_file("/home/bfaber/.julia/dev/NE3DLE/NE3DLE.in")
nml = NE3DLE.read_par_file("/home/bfaber/.julia/dev/NE3DLE/NE3DLE.in")
eq = call_NE3DLE(nml)

Ne3dle_wrapper = StellOpt.EquilibriumWrapper(nml, eq, fd)
#eq = call_NE3DLE(nml)
#StellOpt.update_equilibrium!(Ne3dle_wrapper, eq)
StellOpt.add_equilibrium_wrapper!(prob, Ne3dle_wrapper)
Ne3dle_s = StellOpt.@add_derived_geometry!(prob,NE3DLESurface(equilibrium=Ne3dle_wrapper))
StellOpt.@add_target!(prob, NE3DLE.quasisymmetry_deviation(Ne3dle_s), 0.0, 1.0, "qs_ne3dle")
StellOpt.@add_target!(prob, NE3DLE.magnetic_well(Ne3dle_s), -0.1, 1.0, "well_ne3dle")

StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(0,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(0,2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(0,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(0,2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(1,-2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(1,-1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(1,0)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(1,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(1,2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(1,-2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(1,-1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(1,0)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(1,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(1,2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(2,-2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(2,-1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(2,0)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(2,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Rmn(2,2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(2,-2)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(2,-1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(2,0)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(2,1)")
StellOpt.add_variable!(prob, Ne3dle_wrapper, "Zmn(2,2)")

#residual = StellOpt.optimize_threads(prob)