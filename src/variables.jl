
#struct FiniteDiff <: AbstractDerivative end
struct AutoDiff <: AbstractDerivative end

"""
    OptimizationVariable{T, D} <: AbstractOptVariable

Mutable composite type for holding unconstrained optimization variables
with numeric type `T` and derivative type `D`.

# Fields:
- `name::Symbol`  # Symbol with the variable name
- `value::T`  # Current value
- `weight::T`  # Weight
- `bounds::Vector{T}`  # Vector holding the upper and lower bounds of the variable
- `active::Bool`  # Flag indicating whether variable is active for currrent iteration
- `recompute::Bool`  # Flag indicating if equilibrium recomputation is required upon change of value
"""
mutable struct OptimizationVariable{T} <: AbstractOptVariable
  name::Symbol
  value::T
  weight::T
  bounds::Vector{T}
  equilibrium_key::UInt
  active::Bool
  recompute::Bool
  OptimizationVariable() = OptimizationVariable{Float64}()
  OptimizationVariable{T}() where {T} = new{T}()
  OptimizationVariable{T}(n,v,w,b,k,a,r) where {T} = new{T}(n,v,w,b,k,a,r)
end

function OptimizationVariable(name::Symbol,
                              value=nothing,
                              equilibrium_key=zero(UInt);
                              weight=1.0,
                              bounds = [-Inf, Inf],
                              active=true,
                              recompute=true,
                             )
    if isnothing(value)
        error(ArgumentError("Incorrect variable specification, variable must have an initial value"))
    end
    lower_bound = bounds[1]
    upper_bound = bounds[2]
    promoted_value, promoted_weight, promoted_lb, promoted_ub = promote(value, weight, lower_bound, upper_bound)
    T = typeof(promoted_value)
    return OptimizationVariable{T}(name, promoted_value, promoted_weight, 
                                   [promoted_lb, promoted_ub],
                                   equilibrium_key, active, recompute)
end

function OptimizationVariable(name::AbstractString,
                               value=nothing,
                               equilibrium_key=zero(UInt);
                               kwargs...)
  return OptimizationVariable(Symbol(name), value, equilibrium_key; kwargs...)
end

@inline function name(v::OptimizationVariable)
    String(v.name)
end