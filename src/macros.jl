"""
    @target_parameters

Generate a dictionary of input parameters for an optimization target
"""
macro target_parameters(exprs...)
  args = Vector{Expr}(undef, length(exprs))
  for (i, ex) in enumerate(exprs)
      if Meta.isexpr(ex, :(=)) || Meta.isexpr(ex, :(=>))
          args[i] = Expr(:call, :(=>), QuoteNode(ex.args[1]), eval(ex.args[2]))
      else
          error("Argument requires assignment")
      end
  end
  return :(Dict{Symbol, Any}(eval.($args)...))
end

"""
    @add_derived_geometry!(prob, method)

Construct a derived geometry expression from a method signature provided in 
`method.`  The method signature *must* have keyword arugment 
`equilibrium = eq_wrapper`, where `eq_wrapper` is a previously 
defined `EquilibriumWrapper` variable that has been added to 
the optimization problem `prob` through `add_equilibrium_wrapper!`.
The derived geometry expression is added to both the `prob.derived_geometries`
dictionary and the corresponding `eq_wrapper.derived_geometries`
dictionary.  The returned value is the hash of the `method` expression
that is used as key value for the `derived_geometries` dictionaries.

See also: [`add_equilibrium_wrapper!`](@ref)

Example:
```julia-repl
julia> using MPI, VMEC, StellaratorOptimization

julia> prob = StellOpt.Problem{Float64}();

julia> vmec_wrapper = EquilibriumWrapper(input_dictionary);

julia> add_equilibrium_wrapper!(prob, vmec_wrapper);

julia> vmec_surface = @add_derived_geometry!(prob, VmecSurface(0.5, equilibrium = vmec_wrapper));

julia> (typeof(vmec_surface) <: UInt, haskey(prob.derived_geometries, vmec_surface))
(true, true)
"""
macro add_derived_geometry!(args...)
  prob = args[1]
  geom_expr = args[2]
  geom_call = Expr(:quote, geom_expr)
  geom_slot = findfirst(x -> Meta.isexpr(x, :kw) && x.args[1] === :equilibrium, geom_expr.args)
  parent_geom = geom_expr.args[geom_slot].args[2]
  geom_expr.args[geom_slot] = :equilibrium
  geom_expr = Expr(:quote, geom_expr)
  mod = @__MODULE__
  return esc(quote
    $mod.add_derived_geometry!($prob, hash($geom_call), $mod.DerivedGeometryWrapper($geom_expr, objectid($parent_geom)))
  end)
end

"""
    @add_target!(prob, method, target, weight, label)

Construct a target from a method signature provided in `method.`  The
method signature *must* have keyword arugment `geometry = geometry_key`,
where `geometry_key` is the identifier returned by `@add_derived_geometry`.

Example:
```julia-repl
julia> using MPI, VMEC, StellaratorOptimization

julia> prob = StellOpt.Problem{Float64}();

julia> vmec_wrapper = EquilibriumWrapper(input_dictionary);

julia> add_equilibrium_wrapper!(prob, vmec_wrapper);

julia> vmec_surface = @add_derived_geometry!(prob, VmecSurface(0.5, equilibrium = vmec_wrapper));

julia> @add_target!(prob, VMEC.quasisymmetry_deviation(1, 4, 16, 16, geometry = vmec_surface), 0.0, 1.0, "qs(0.5)");
```
"""
macro add_target!(prob, method, target, weight, label)
  mod = @__MODULE__

  #method_call = Expr(method.head, method.args...)
  #sym, index = find_last_module(method)
  #method_call.args[1] = insert_main(method, sym, index)
  method_expr = Expr(:quote, method)
  return esc(quote
        # Need to evaluate an argument in the calling scope
        # so the values can be passed to the calling function
        # The first argument will always be the function to be called
        method_call = Expr($method_expr.head, $method_expr.args...)
        calling_mod, index = $mod._find_calling_module(method_call)
        escaped_mod = Symbol(@__MODULE__)
        method_call = $mod._insert_escaped_module(method_call, escaped_mod, calling_mod, index)
        for i in 2:length($method_expr.args)
            if Meta.isexpr($method_expr.args[i], :kw)
                method_call.args[i].args[2] = eval($method_expr.args[i].args[2])
            else
                method_call.args[i] = eval($method_expr.args[i])
            end
        end
        geometry_slot = findfirst(x -> x isa UInt, method_call.args)
        geometry_key = !isnothing(geometry_slot) ? method_call.args[geometry_slot] : error("Target must have a UInt identifier for the geometry")
        $mod.add_target!($prob, $mod.Target($target, $weight, method_call, geometry_key, $label))
  end)
  
end

function _find_calling_module(ex::Expr,
                              index = 1;
                             )
    if Meta.isexpr(ex.args[1], :(.))
        return _find_calling_module(ex.args[1], index + 1)
    else
        return ex.args[1], index
    end
end

function _insert_escaped_module(ex::Expr,
                                escaped_sym::Symbol,
                                calling_sym::Symbol,
                                insert_index::Int,
                                index::Int = 1,
                               )
    if index < insert_index
        ex.args[1] = _insert_escaped_module(ex.args[1], escaped_sym, calling_sym, insert_index, index + 1)
        return ex
    elseif index == insert_index
        ex.args[1] = Expr(:(.), escaped_sym, QuoteNode(calling_sym))
        return ex
    end
end
