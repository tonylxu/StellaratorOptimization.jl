module StellaratorOptimization
using LinearAlgebra
using Logging
using Requires
using OrderedCollections
using Polyester
using Printf
using HDF5
using JLD2
#using FiniteDiffferences: central_fdm, forward_fdm
using Distributions: Distributions.Bernoulli
using Optim

using PlasmaEquilibriumToolkit

const StellOpt = StellaratorOptimization
export StellOpt

#outputs
export read_history_h5

export @add_target!, @add_derived_geometry!

const NO_EQUILIBRIUM = 1.0e10

abstract type AbstractOptTarget end;

abstract type AbstractOptVariable end;
   
abstract type AbstractOptEquilibrium end;

abstract type AbstractModuleWrapper end;

abstract type AbstractDerivative end;


include("equilibrium_wrapper.jl")
include("variables.jl")
include("target.jl")
include("problem.jl")
include("macros.jl")
include("utilities.jl")
include("gradient.jl")
include("spsa.jl")
include("optimize_threads.jl")
include("output.jl")
include("process_output.jl")


function __init__()
    @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" begin
        include("utilities_mpi.jl")
        include("macros_mpi.jl")
        include("optimize_mpi.jl")
        include("gradient_mpi.jl")
        include("spsa_mpi.jl")
    end

    @require CairoMakie="13f3f980-e62b-5c42-98c6-ff1f3baf88f0" include("plotting_makie.jl")
    
end


end
