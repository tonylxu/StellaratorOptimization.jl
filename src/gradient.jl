
function gradient(prob::Problem{T};
                 ) where {T}
    G = zeros(T, length(prob.state_vector))
    for (key, wrapper) in prob.equilibrium_wrappers
        @info "Computing the gradient at iteration $(prob.iteration)
        for $(equilibrium_type_string(wrapper)) state variables
        with method $(derivative_type_string(wrapper))"
        var_indices = findall(v -> v.equilibrium_key == key, prob.state_vector)
        eq_targets = filter(p -> prob.derived_geometries[p.second.geometry_key].parent_key == key, prob.targets)
        eq = wrapper.eq
        input = wrapper.input
        objective_gradient, target_gradients = gradient(wrapper, deepcopy(prob.state_vector[var_indices]), deepcopy(eq_targets); iteration = prob.iteration)
        foreach(p -> prob.targets[p.first].gradient = p.second, target_gradients)
        foreach(p -> G[p[2]] = objective_gradient[p[1]], enumerate(var_indices))
        update_equilibrium!(wrapper, eq)
        set_input!(wrapper, input)  
    end
    return G, new_objective, new_targets
end

struct ForwardDiff{T} <: AbstractDerivative
    δ::T
end

struct CenteredDiff{T} <: AbstractDerivative
    δ::T
end

function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_variables::Vector{OptimizationVariable},
                  targets::Dict{Int, Target};
                  kwargs...
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: Union{ForwardDiff, CenteredDiff}}
    nthreads = div(Threads.nthreads(), BLAS.get_num_threads())
    minbatch = div(length(state_variables), nthreads) + 1
println("Parallelizing gradient over $nthreads threads with a batch size >= $minbatch")
    target_gradients = Vector{Dict{Int, Vector{T}}}(undef, nthreads)
    objective_gradient = Vector{Vector{T}}(undef, nthreads)
    for t in 1:nthreads
        target_gradients[t] = Dict{Int, Vector{T}}()
        foreach(k -> target_gradients[t][k] = zeros(T, length(state_variables)), keys(targets))
        objective_gradient[t] = zeros(T, length(state_variables))
    end
   # objective_gradient = fill(zeros(T, length(state_variables)), nthreads)
    @batch minbatch=minbatch per=thread for i in axes(state_variables, 1)
        t = Threads.threadid()
        v = deepcopy(state_variables[i])
        objective_derivative, target_derivatives = gradient(equilibrium_wrapper, v, targets)
        objective_gradient[t][i] = objective_derivative
        for k in keys(targets)
            target_gradients[t][k][i] = target_derivatives[k]
        end
    end
    for t in 2:nthreads
        objective_gradient[1] .+= objective_gradient[t]
        mergewith!(+, target_gradients[1], target_gradients[t])
    end
    return objective_gradient[1], target_gradients[1]
end


function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_var::OptimizationVariable,
                  targets::Dict{Int, Target};
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: ForwardDiff}
    g = equilibrium_wrapper.grad_method
    initial_value = copy(state_var.value)
    @debug "$initial_value"
    wrapper_input = deepcopy(equilibrium_wrapper.input)
    current_values = Dict{Int, T}()
    foreach(p -> current_values[p.first] =  p.second.current_value, targets)
    target_values = empty(current_values)
    target_gradients = empty(current_values)
    objective_gradient = zero(T)
    state_var.value = initial_value + g.δ
    @debug "$state_var"
    translate!(equilibrium_wrapper, [state_var])
    @debug "$(equilibrium_wrapper.input)"
    eq = compute_equilibrium(equilibrium_wrapper)
    
    if !isnothing(eq)
        update_equilibrium!(equilibrium_wrapper, eq)
        push!(target_values, compute_targets(targets, equilibrium_wrapper)...)
        for key in keys(targets)
            target_gradients[key] = (target_values[key] - current_values[key]) / g.δ
            objective_gradient += ((targets[key].target_value - target_values[key])^2/targets[key].target_weight^2 -
                                  (targets[key].target_value - current_values[key])^2/targets[key].target_weight^2) / g.δ
        end
    else
        foreach(k -> target_gradients[k] = convert(T, NaN), keys(targets))
        objective_gradient += NaN
    end
    set_input!(equilibrium_wrapper, wrapper_input)

    return objective_gradient, target_gradients 

end


function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_var::OptimizationVariable,
                  targets::Dict{Int, Target};
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: CenteredDiff}
    g = equilibrium_wrapper.grad_method
    initial_value = copy(state_var.value)
    @debug "$initial_value"
    wrapper_input = deepcopy(equilibrium_wrapper.input)
    current_values = Dict{Int, T}()
    foreach(p -> current_values[p.first] =  p.second.current_value, targets)
    forward_targets = empty(current_values)
    backward_targets = empty(current_values)
    target_gradients = empty(current_values)
    objective_gradient = zero(T)
    state_var.value = initial_value + g.δ
    @debug "$state_var"
    translate!(equilibrium_wrapper, [state_var])
    @debug "$(equilibrium_wrapper.input)"
    eq = compute_equilibrium(equilibrium_wrapper)

    if !isnothing(eq)
        update_equilibrium!(equilibrium_wrapper, eq)
        push!(forward_targets, compute_targets(targets, equilibrium_wrapper)...)

        set_input!(equilibrium_wrapper, wrapper_input)
        state_var.value = initial_value - g.δ
        translate!(equilibrium_wrapper, [state_var])
        eq = compute_equilibrium(equilibrium_wrapper)

        if !isnothing(eq)
            update_equilibrium!(equilibrium_wrapper, eq)
            push!(backward_targets, compute_targets(targets, equilibrium_wrapper)...)
            for key in keys(targets)
                forward_objective = (targets[key].target_value - forward_targets[key])^2/targets[key].target_weight^2
                backward_objective = (targets[key].target_value - backward_targets[key])^2/targets[key].target_weight^2
                target_gradients[key] = 0.5 * (forward_targets[key] - backward_targets[key]) / g.δ
                objective_gradient += 0.5 * (forward_objective - backward_objective) / g.δ
            end
        else
            throw(ErrorException("Cannot compute centered difference gradient for 
                                  state variable $(name(state_var)), try adjusting
                                  the finite difference step size lower"))
        end
    else
        throw(ErrorException("Cannot compute centered difference gradient for 
                              state variable $(name(state_var)), try adjusting
                              the finite difference step size lower"))
    end
    set_input!(equilibrium_wrapper, wrapper_input)

    return objective_gradient, target_gradients
end
