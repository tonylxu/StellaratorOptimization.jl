
"""
    optimize_mpi(prob::Problem,comms::OptComms)

Run an optimization problem defined by `prob` using MPI with communicators
defined in `comms`.
"""
function optimize_mpi(prob::Problem{T},
                      comms::OptComms;
                     ) where {T}
    if comms.is_world_head
        # Define the callback function that requires knowledge of the problem
        function cb_function(x)
            update_history!(x, prob)
        end

        # Extract initial state vector of BitType data
        initial_state_vector = state_vector_values(prob)

        # Set the intial gradient of each target to zero
        for i in 1:length(prob.targets)
            prob.targets[i].gradient = zeros(T, length(prob.state_vector))
        end

        # Augment the arguments of the optimizer options with the callback
        option_args = Dict{Symbol, Any}(:callback => cb_function)
        for f in fieldnames(Optim.Options)
            val = getfield(prob.optimizer.options, f)
            if !isnothing(val)
                option_args[f] = val
            end
        end
        if !option_args[:extended_trace]
            option_args[:extended_trace] = true
        end
        if !option_args[:store_trace]
            option_args[:store_trace] = true
        end
        new_options = (; option_args...)

        write_history_attributes(prob)  
        log_io = open(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_log.jl"), "w+")
        logger = SimpleLogger(log_io, Logging.Info)
        residual = nothing
        try
            residual = with_logger(logger) do
                Optim.optimize(Optim.only_fg!((F, G, θ) -> objective_gradient!(F, G, θ, prob, comms; logger_io = log_io)),
                            initial_state_vector, prob.optimizer.method, Optim.Options(; new_options...))
            end
            flush(log_io)
            close(log_io)
            JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_problem.jld2"), prob)
            JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_residual.jld2"), residual)
        catch
            JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_problem.jld2"), prob)
        end
            #write_history_file(prob)
        MPI.bcast(true, 0, comms.world)
        return residual
    else
        if is_computable(comms.compute)
            worker_loop(prob, comms)
        end
        return nothing
    end
end

"""
    worker_loop(prob::Problem,comms::OptComms)

Listens for optimization state vector and calculation termination from the
global head node.  Exits when a finished value is broadcast from the global
head node.
"""
function worker_loop(prob::Problem{T},
                     comms::OptComms;
                    ) where {T}
    finished = false
    target_gradients = Dict{Int, Vector{T}}()
    while !finished
        finished = MPI.bcast(nothing, 0, comms.world)
        if !finished
            θ = MPI.bcast(nothing, 0, comms.world)
            adjust_state_vector!(prob, θ)
            for _ in length(prob.equilibrium_wrappers)
                wrapper = MPI.bcast(nothing, 0, comms.world)
                var_indices = MPI.bcast(nothing, 0, comms.world)
                eq_targets = MPI.bcast(nothing, 0, comms.world)
                objective_gradient!(nothing, nothing, wrapper,
                                    prob.state_vector[var_indices],
                                    eq_targets, target_gradients, comms) 
            end
        end
    end
    return nothing
end

"""
    objective_gradient!(F, G, x::AbstractVector, prob::Problem, comms::OptComms[; logger_io::Union{Nothing, IOStream} = nothing])

MPI parallelized function for computing the objective and the gradient from independent worker group calculations.
The world head rank passes the updated state vector and the equilibrium to compute to the worker groups.
""" 
function objective_gradient!(F,
                             G,
                             x::AbstractVector,
                             prob::Problem{T},
                             comms::OptComms;
                             logger_io::Union{Nothing, IOStream} = nothing,
                            ) where {T}
    !isnothing(logger_io) ? flush(logger_io) : nothing
    clamp!(x, prob.state_vector)
    # If the loop is in this function there is still work left to do; bcast false
    MPI.bcast(false, 0, comms.world)
    MPI.bcast(x, 0, comms.world)
    adjust_state_vector!(prob, x)
    obj, grad = (zero(T), nothing)
    for (key, wrapper) in prob.equilibrium_wrappers
        var_indices = findall(v -> v.equilibrium_key == key, prob.state_vector)
        eq_targets = filter(p -> prob.derived_geometries[p.second.geometry_key].parent_key == key, prob.targets)
        MPI.bcast(wrapper, 0, comms.world)
        MPI.bcast(var_indices, 0, comms.world)
        MPI.bcast(eq_targets, 0, comms.world)

        grad_vec = !isnothing(G) ? Vector{T}(undef, length(var_indices)) : nothing 
        target_gradients = Dict{Int, Vector{T}}()

        obj_part = objective_gradient!(F, grad_vec, wrapper,
                                       prob.state_vector[var_indices],
                                       eq_targets, target_gradients, comms) 
        if !isnothing(F) && !isnothing(obj_part)
            obj += obj_part
        end    

        if !isnothing(G) && ((!isnothing(F) && !isnothing(obj_part)) || isnothing(F))
            foreach(p -> G[p[2]] = copy(grad_vec[p[1]]), enumerate(var_indices))
            for t in keys(eq_targets)
                copyto!(prob.targets[t].gradient, target_gradients[t])
            end
        end
    end
    if !isnothing(G)
        adjust_gradient!(prob, copy(G))
    end
    if !isnothing(F)
        adjust_objective!(prob, obj)
        return obj
    end
    return nothing
end 


"""
    objective_gradient!(F, G, wrapper::EquilibriumWrapper, state_vector::AbstractVector{OptimizationVariable}, targets::Dict{Int, Target}, target_gradients::Dict{Int, Vector{T}}, comms::OptComms)

Compute the objective and gradient terms for the objective function targets associated with the wrapped equilibrium `wrapper` specified by `targets`.  The entries of `state_vector` can be a subset of all the state vector entries associated with `wrapper`.
"""
function objective_gradient!(F,
                             G,
                             wrapper::EquilibriumWrapper{T, E, D},
                             state_vector::AbstractVector{OptimizationVariable},
                             targets::Dict{Int, Target},
                             target_gradients::Dict{Int, Vector{T}},
                             comms::OptComms; 
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: Union{CenteredDiff, ForwardDiff}}
    translate!(wrapper, state_vector)
    if comms.is_world_head
        obj, grad = (nothing, nothing)
        if !isnothing(F) 
            MPI.bcast(true, 0, comms.world)
            obj = compute_objective(wrapper, state_vector, targets, comms)
        else
            MPI.bcast(false, 0, comms.world)
        end
        if !isnothing(G) && ((!isnothing(F) && !isnothing(obj)) || isnothing(F))
            MPI.bcast(true, 0, comms.world) 
            grad, target_derivatives = gradient(wrapper, deepcopy(state_vector), targets, comms)
            copyto!(G, grad)
            for key in keys(target_derivatives)
                target_gradients[key] = copy(target_derivatives[key])
            end
        else
            MPI.bcast(false, 0, comms.world)
        end
        return obj
    else
        objective_needed = MPI.bcast(nothing, 0, comms.world)
        if objective_needed
            compute_objective(wrapper, state_vector, targets, comms)
        end
        gradient_needed = MPI.bcast(nothing, 0, comms.world)
        if gradient_needed
            gradient(wrapper, deepcopy(state_vector), targets, comms)
        end
    end
end

"""
function compute_objective(wrapper::EquilibriumWrapper, state_vector::AbstractVector{OptimiziationVariable}, targets::Dict{Int, Target}, comms::OptComms)

Computes the target objective functions for the wrapped equilibrium `wrapper` specified by `targets`.  The entries in `state_vector` can be a subset of all the state vector quantities associated with `wrapper`.
"""
function compute_objective(wrapper::EquilibriumWrapper{T, E, D},
                           state_vector::AbstractVector{OptimizationVariable},
                           targets::Dict{Int, Target},
                           comms::OptComms;
                          ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
   if !comms.is_world_head
        eq = compute_equilibrium(wrapper, comms.compute.comm)
        if !isnothing(eq)
            if comms.group_id == 1 && comms.is_compute_head
                MPI.send(true, 0, 0, comms.inter_group.comm)
            end
            update_equilibrium!(wrapper, eq)
            target_values = compute_targets(targets, wrapper, comms)
            target_values = MPI.bcast(target_values, 0, comms.compute)
            if comms.is_compute_head
                F = zero(T)
                for key in keys(target_values)
                    F += (targets[key].target_value - target_values[key])^2/targets[key].target_weight^2 
                    targets[key].current_value = target_values[key]
                end
                if comms.group_id == 1
                    MPI.send(eq, 0, 1, comms.inter_group.comm)
                    MPI.send(target_values, 0, 2, comms.inter_group.comm)
                    MPI.send(F, 0, 3, comms.inter_group.comm)
                end
            else
                for key in keys(target_values)
                    targets[key].current_value = target_values[key]
                end
            end
        else
            if comms.group_id == 1 && comms.is_compute_head
                MPI.send(false, 0, 0, comms.inter_group.comm)
            end
        end 
        return nothing
    else
        success, _ = MPI.recv(1, 0, comms.inter_group.comm)
        if success
            eq, _ = MPI.recv(1, 1, comms.inter_group.comm)
            target_values, _ = MPI.recv(1, 2, comms.inter_group.comm)
            F, _ = MPI.recv(1, 3, comms.inter_group.comm)
            update_equilibrium!(wrapper, eq)
            for key in keys(target_values)
                targets[key].current_value = target_values[key]
            end
            return F
        else
            return nothing
        end
    end
end

#=
function least_squares_objective(prob::Problem{T};
                                 gradient_index::Union{Nothing, Int} = nothing,
                                 finite_diff_step::Union{Nothing, Int} = nothing
                                ) where {T}
    obj = zero(T)
    for (index, target) in prob.targets
        #length(target.label) < 25 ? target_label = target.label * repeat(" ", 25-length(target.label)) : target_label = target.label
        #target_string = @sprintf "%.3E" target.target_value
        #value_string = @sprintf "%.5E" target.current_value
        #sigma_string = @sprintf "%.3E" target.target_weight
        #println(target_label,": target ",target_string," value ",value_string," sigma ",sigma_string)
        if isa(target.target_value, Tuple) || isa(target.target_value, AbstractVector)
            for i = 1:length(target.target_value)
                obj += (target.target_value[i] - target.current_value[i])^2 / (target.target_weight[i]^2)
            end
        else
            obj += (target.target_value - target.current_value)^2 / (target.target_weight^2)
        end
    end
    objective_string = @sprintf "%.5E" obj
    #println("Objective value: ",objective_string)
    return obj
end
=#

function broadcast_problem!(prob::Problem{T},
                            root::Int,
                            comm_info::CommInfo;
                           ) where {T}
    if comm_info.rank == root
        for field in fieldnames(Problem)
            MPI.bcast(field, root, comm_info)
            MPI.bcast(getfield(prob, field), root, comm_info)
        end
        MPI.bcast(:finished, root, comm)
    else
        field = MPI.bcast(nothing, root, comm_info)
        while field in fieldnames(Property)
            data = MPI.bcast(nothing, root, comm_info)
            setfield!(prob, field, data)
            field = MPI.bcast(nothing, root, comm_info)
        end
    end
    return nothing
end
