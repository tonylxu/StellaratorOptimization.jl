
function gradient(prob::Problem{T},
                  comms::OptComms;
                 ) where {T}
    world_rank = comms.world.rank
    world_size = comms.world.size
    compute_rank = comms.compute.rank
    G = comms.is_world_head ? zeros(T, length(prob.state_vector)) : nothing
    finished_loop = false

    if comms.is_world_head
        for (key, wrapper) in prob.equilibrium_wrappers
            @info "Computing the gradient at iteration $(prob.iteration)
            for $(equilibrium_type_string(wrapper)) state variables
            with method $(derivative_type_string(wrapper))"

            var_indices = findall(v -> v.equilibrium_key == key, prob.state_vector)
            eq_targets = filter(p -> prob.derived_geometries[p.second.geometry_key].parent_key == key, prob.targets)

            MPI.bcast(false, 0, comms.world)
            MPI.bcast(wrapper, 0, comms.world)
            MPI.bcast(var_indices, 0, comms.world)
            MPI.bcast(eq_targets, 0, comms.world)
            MPI.bcast(prob.iteration, 0, comms.world)

@debug "Rank $(comms.world.rank) in iteration $(prob.iteration) calling the
$(derivative_type_string(wrapper)) on wrapper $(equilibrium_type_string(wrapper))
for state variables $var_indices"

            objective_gradient, target_gradients =
                gradient(deepcopy(wrapper), deepcopy(prob.state_vector[var_indices]),
                         deepcopy(eq_targets), comms; iteration = prob.iteration)
            for (i, v) in enumerate(var_indices)
                G[v] = objective_gradient[i] 
                for (k, t) in target_gradients
                    prob.targets[k].gradient[v] = t[i]
                end
            end
        end
        MPI.bcast(true, 0, comms.world)
        return G
    else
        while !finished_loop
            finished_loop = MPI.bcast(nothing, 0, comms.world)
            if !finished_loop
                wrapper = MPI.bcast(nothing, 0, comms.world)
                var_indices = MPI.bcast(nothing, 0, comms.world)
                eq_targets = MPI.bcast(nothing, 0, comms.world)
                iteration = MPI.bcast(nothing, 0, comms.world)

@debug "$(comms.world.rank) 
 in iteration $iteration calling the $(derivative_type_string(wrapper)) method on wrapper $(equilibrium_type_string(wrapper))
for state variables $var_indices"

                gradient(deepcopy(wrapper), deepcopy(prob.state_vector[var_indices]),
                         deepcopy(eq_targets), comms; iteration = iteration)
            end
        end
        return nothing
    end
end

"""
    gradient(x::AbstractVector,prob::Problem,comms::OptComms)

The main function for computing the objective function and gradient value using MPI.
The global head node acts as a controller, sending worker groups which gradient index
to evaluate and gathering the results.  Only the global head node returns value of the
objective and gradient to the optimization algorithm.
"""
function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_variables::Vector{OptimizationVariable},
                  targets::Dict{Int, Target},
                  comms::OptComms;
                  kwargs...
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: Union{ForwardDiff, CenteredDiff}}
    world_rank = comms.world.rank
    group_rank = comms.is_compute_head || comms.is_world_head ? comms.inter_group.rank : nothing
    compute_rank = comms.compute.rank
    index = zero(Int)

    if group_rank != 0
        finished_eval = false 
        # This is a worker, doing the calculations
        if comms.is_compute_head 

            recv_index_from_head_tag = 18323 + group_rank
            notify_head_tag = 21853 + group_rank
            send_result_to_head_tag = 19293 + group_rank
            send_targets_to_head_tag = 14293 + group_rank

            while !finished_eval
                index, _ = MPI.recv(0, recv_index_from_head_tag, comms.inter_group.comm)
                MPI.bcast(index, 0, comms.compute)

                if index >= 1
                    # Do the calculation
                    var = state_variables[index]
                    objective_derivative, target_derivatives = 
                        gradient(equilibrium_wrapper, deepcopy(var), targets, comms)
                    MPI.send(group_rank, 0, notify_head_tag, comms.inter_group.comm)
                    # Send the results back to the global head
                    MPI.send(objective_derivative, 0, send_result_to_head_tag, comms.inter_group.comm)
                    # Note this is a blocking send
                    MPI.send(target_derivatives, 0, send_targets_to_head_tag, comms.inter_group.comm)
                else
                    finished_eval = true
                end
            end
        else
            while !finished_eval
                index = MPI.bcast(nothing, 0, comms.compute)
                if index >= 1
                    # Do the calculation
                    # index gets passed here, but instead split this up so that you can Mpi_send prob.equilibrium
                    var = state_variables[index]
                    _, _ = gradient(equilibrium_wrapper, deepcopy(var), targets, comms)
                else
                    finished_eval = true
                end
            end
        end
        return nothing
    elseif comms.is_world_head
        n_worker_groups = comms.inter_group.size - 1

        recv_result_from_worker_tags = 19293 .+ collect(1:n_worker_groups)
        recv_targets_from_worker_tags = 14293 .+ collect(1:n_worker_groups)
        notified_head_tags = 21853 .+ collect(1:n_worker_groups)
        send_index_to_worker_tags = 18323 .+ collect(1:n_worker_groups)

        gradient_indices = world_rank == 0 ? collect(1:length(state_variables)) : nothing
        target_gradients = world_rank == 0 ? Dict{Int, Vector{T}}() : nothing
        target_gradient_buffer = world_rank == 0 ? Dict{Int, Vector{T}} : nothing
        foreach(k -> target_gradients[k] = zeros(T, length(state_variables)), keys(targets))
        index_map = Dict{Int, Int}()
        G = zeros(T, length(state_variables))


        finished_eval = false
        proc_status = fill(true, n_worker_groups)
        for group_index = 1:n_worker_groups
            grad_index = findfirst(x -> x != -1, gradient_indices)
            if !isnothing(grad_index)
                # Send out the gradient index to evaluate
                index = gradient_indices[grad_index]
                index_map[group_index] = gradient_indices[grad_index]
                gradient_indices[grad_index] = -1
                MPI.send(index, group_index,
                         send_index_to_worker_tags[group_index], comms.inter_group.comm)

            else
                index = -1
                MPI.send(index, group_index,
                         send_index_to_worker_tags[group_index], comms.inter_group.comm)
                proc_status[group_index] = false
            end
        end

        finished_eval = all(i -> !i, proc_status)

        while !finished_eval
            group_index, _ = MPI.recv(MPI.MPI_ANY_SOURCE, MPI.MPI_ANY_TAG, comms.inter_group.comm)
            objective_derivative, _ = MPI.recv(group_index, recv_result_from_worker_tags[group_index], comms.inter_group.comm)
            target_gradient_buffer, _ = MPI.recv(group_index, recv_targets_from_worker_tags[group_index], comms.inter_group.comm)        
            grad_index = index_map[group_index]
            G[grad_index] = objective_derivative
            for key in keys(target_gradient_buffer)
                target_gradients[key][grad_index] = target_gradient_buffer[key]
            end
            index = findfirst(x -> x != -1, gradient_indices)

            if !isnothing(index)
                index_map[group_index] = index
                MPI.send(index, group_index, send_index_to_worker_tags[group_index], comms.inter_group.comm)
                gradient_indices[index] = -1
            else
                index = -1
                MPI.send(index, group_index, send_index_to_worker_tags[group_index], comms.inter_group.comm)
                proc_status[group_index] = false
            end
            if all(i -> !i, proc_status)
                finished_eval = true
            end
        end
        return G, target_gradients
    end
end


function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_var::OptimizationVariable,
                  targets::Dict{Int, Target},
                  comms::OptComms;
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: ForwardDiff}
    rank = comms.compute.rank
    g = equilibrium_wrapper.grad_method
    initial_value = copy(state_var.value)
    wrapper_input = deepcopy(equilibrium_wrapper.input)
    current_values = rank == 0 ? Dict{Int, T}() : nothing
    target_values = rank == 0 ? empty(current_values) : nothing
    target_gradients = rank == 0 ? empty(current_values) : nothing
    objective_gradient = rank == 0 ? zero(T) : nothing
    state_var.value = initial_value + g.δ
    translate!(equilibrium_wrapper, [state_var])
    eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)
    
    if !isnothing(eq)
        update_equilibrium!(equilibrium_wrapper, eq)
        if rank == 0
            foreach(p -> current_values[p.first] =  p.second.current_value, targets)
            push!(target_values, compute_targets(targets, equilibrium_wrapper, comms)...)
            for key in keys(targets)
                target_gradients[key] = (target_values[key] - current_values[key]) / g.δ
                objective_gradient += ((targets[key].target_value - target_values[key])^2/targets[key].target_weight^2 -
                                  (targets[key].target_value - current_values[key])^2/targets[key].target_weight^2) / g.δ
            end
        else
            compute_targets(targets, equilibrium_wrapper, comms)
        end
    else
        if rank == 0
            foreach(k -> target_gradients[k] = convert(T, NaN), keys(targets))
            objective_gradient += NaN
        end
    end
    set_input!(equilibrium_wrapper, wrapper_input)

    return objective_gradient, target_gradients 

end


function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_var::OptimizationVariable,
                  targets::Dict{Int, Target},
                  comms::OptComms;
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: CenteredDiff}
    rank = comms.compute.rank
    g = equilibrium_wrapper.grad_method
    initial_value = copy(state_var.value)
    wrapper_input = deepcopy(equilibrium_wrapper.input)
    current_values = rank == 0 ? Dict{Int, T}() : nothing
    forward_targets = rank == 0 ? empty(current_values) : nothing
    backward_targets = rank == 0 ? empty(current_values) : nothing
    target_gradients = rank == 0 ? empty(current_values) : nothing
    objective_gradient = rank == 0 ? zero(T) : nothing
    state_var.value = initial_value + g.δ
    translate!(equilibrium_wrapper, [state_var])
    eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)

    if !isnothing(eq)
        update_equilibrium!(equilibrium_wrapper, eq)
        if rank == 0
            foreach(p -> current_values[p.first] =  p.second.current_value, targets)
            push!(forward_targets, compute_targets(targets, equilibrium_wrapper, comms)...)

            set_input!(equilibrium_wrapper, wrapper_input)
            state_var.value = initial_value - g.δ
            translate!(equilibrium_wrapper, [state_var])
            eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)

            if !isnothing(eq)
                update_equilibrium!(equilibrium_wrapper, eq)
                push!(backward_targets, compute_targets(targets, equilibrium_wrapper, comms)...)
                for key in keys(targets)
                    target_gradients[key] = (forward_targets[key] - backward_targets[key]) / g.δ
                    forward_objective = (targets[key].target_value - forward_targets[key])^2/targets[key].target_weight^2
                    backward_objective = (targets[key].target_value - backward_targets[key])^2/targets[key].target_weight^2
                    objective_gradient += 0.5 * (forward_objective - backward_objective) / g.δ
                end
            else
                throw(ErrorException("Cannot compute centered difference gradient for 
                state variable $(name(state_var)), try adjusting
                the finite difference step size lower"))
            end
        else
            compute_targets(targets, equilibrium_wrapper, comms)
            set_input!(equilibrium_wrapper, wrapper_input)
            state_var.value = initial_value - g.δ
            translate!(equilibrium_wrapper, [state_var])
            eq = compute_equilibrium(equilibrium_wrapper, comms.compute.comm)

            if !isnothing(eq)
                update_equilibrium!(equilibrium_wrapper, eq)
                compute_targets(targets, equilibrium_wrapper, comms)
            else
                throw(ErrorException("Cannot compute centered difference gradient for 
                state variable $(name(state_var)), try adjusting
                the finite difference step size lower"))
            end
        end
    else
        throw(ErrorException("Cannot compute centered difference gradient for 
        state variable $(name(state_var)), try adjusting
        the finite difference step size lower"))
    end
    set_input!(equilibrium_wrapper, wrapper_input)

    return objective_gradient, target_gradients 

end
