function adjust_time!(prob::Problem, 
                      i::Int,
                      time::Float64;
                    )
    prob.targets[i].runtime = copy(time)
end

# Helper function for adjusting problem value
function adjust_target_value!(prob::Problem, 
                              i::Int,
                              value;
                             )
    prob.targets[i].current_value = copy(value)
end

function adjust_state_vector!(prob::Problem,
                              new_values::Vector;
                             )
    for var_index in eachindex(prob.state_vector)
        set_variable_value!(prob, var_index, new_values[var_index])
    end
    return nothing
end

function adjust_state_vector!(prob::Problem,
                              index::Int,
                              value;
                             )
    prob.state_vector[index].value = value
    return nothing
end

function adjust_objective!(prob::Problem,
                           objective::T;
                          ) where {T}
    prob.objective = copy(objective)
end

function adjust_gradient!(prob::Problem,
                          gradient::Vector{T};
                         ) where {T}
    prob.gradient = copy(gradient)
end

function update_history!(prob::Problem,
                         new_record::OptHistory
                        )
    push!(prob.history, new_record)
end

function update_history!(state,
                         prob::Problem{T};
                        ) where {T}
    current_state = typeof(state) <: Array ? last(state) : state
    θ = haskey(current_state.metadata, "x") ? current_state.metadata["x"] : current_state.metadata["centroid"]

    write_history_record(prob)
    for i in eachindex(prob.equilibrium_wrappers)
        eq_state_var_inds = findall(v -> v.equilibrium_key ==  i, prob.state_vector)
        eq_state_vars = empty(prob.equilibrium_wrappers[i].input)
        key_type = keytype(eq_state_vars)
        foreach(i -> eq_state_vars[key_type(prob.state_vector[i].name)] = θ[i], eq_state_var_inds)
        set_input!(prob.equilibrium_wrappers[i], OrderedDict(eq_state_vars))
        if mod(current_state.iteration - 1, prob.write_input_interval) == 0
            eq_mod = lowercase(equilibrium_type_string(prob.equilibrium_wrappers[i]))
            filename = joinpath(prob.output_directory, "input_$(eq_mod).opt_$(current_state.iteration)")
            @debug "Writing the $(equilibrium_type_string(prob.equilibrium_wrappers[i])) equilibrium input file for iteration $(current_state.iteration)"
            write_input_file(prob.equilibrium_wrappers[i], filename)
            @debug "Wrote the $(equilibrium_type_string(prob.equilibrium_wrappers[i])) equilibrium input file for iteration $(current_state.iteration)"       
        end
    end
    prob.iteration = current_state.iteration + 1
    @info "Completed iteration #$(current_state.iteration)
    ===================================================================================
    Objective value: $(current_state.value)   Gradient norm: $(current_state.g_norm)\n"
    println("Completed iteration #$(current_state.iteration)\n===================================================================================\nObjective value: $(current_state.value)   Gradient norm: $(current_state.g_norm)\n")
    return false
end

function history(prob::Problem,
                 iter::Int;
                )
    return prob.history[iter]
end

function last_state_vector(prob::Problem)
    return prob.history[end].state_vector
end

function last_gradients(prob::Problem)
    return prob.history[end].gradients
end

function add_target!(prob::Problem,
                     target::AbstractOptTarget;
                    )
    next_index = prob.targets.count + 1
    prob.targets[next_index] = target
    return next_index
end

function add_variable!(prob::Problem,
                       var::AbstractOptVariable;
                      )
    push!(prob.state_vector, var)
    return length(prob.state_vector)
end


function add_variable!(::Problem{T},
                       ::EquilibriumWrapper{T, E, D},
                       ::Union{AbstractString, Symbol};
                       kwargs...
                      ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("Method for adding state vector variable from equilibrium $(E) not implemented")
end

function add_multiple_modes!(::Problem{T},
                             ::EquilibriumWrapper{T, E, D},
                             ::Int,
                             ::Int;
                             kwargs...
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("Method for adding multiple modes from equilibrium $(E) not implemented")
end

function state_vector_values(prob::Problem{T}) where {T}
    values = Vector{T}(undef, length(prob.state_vector))
    for i in eachindex(values, prob.state_vector)
        values[i] = copy(prob.state_vector[i].value)
    end
    return values
end

function add_optimizer!(prob::Problem,
                        optimizer::Optimizer
                       )
    setfield!(prob, :optimizer, optimizer)
    return nothing
end

function set_variable_value!(prob::Problem,
           var_index::Int,
           value::T;
          ) where {T}
    setfield!(prob.state_vector[var_index], :value, value)
    return nothing
end

function add_equilibrium_wrapper!(prob::Problem,
                                  wrapped_eq::EquilibriumWrapper{T, E, D};
                                 ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    key = objectid(wrapped_eq)
    if !haskey(prob.equilibrium_wrappers, key)
        prob.equilibrium_wrappers[key] = wrapped_eq
    end
    return key
end

function update_equilibrium_wrapper!(prob::Problem,
                                     key::UInt,
                                     new_wrapper::EquilibriumWrapper{T, E, D};
                                    ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
  prob.equilibrium_wrappers[key] = new_wrapper
  return nothing
end

function get_equilibrium_wrapper_target_ids(wrapper::EquilibriumWrapper{T, E, D},
                                            targets::Dict{Int, Target};
                                           ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    target_ids = Vector{Int}(undef, 0)
    for (key, _) in wrapper.derived_geometries
        for (i, target) in targets
            if target.geometry_key == key
                push!(target_ids, i)
            end
        end
    end
    return target_ids
end

function get_equilibrium_wrapper_variable_ids(wrapper::EquilibriumWrapper{T, E, D},
                                              variables::Vector{OptimizationVariable{T}};
                                             ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    key = first(wrapper.derived_geometries).parent_key
    return filter(i -> variables[i].equilibrium_key == key, 1:length(variables))
end

function add_derived_geometry!(prob::Problem,
                               derived_key::UInt,
                               derived_wrapper::DerivedGeometryWrapper;
                              )
    prob.derived_geometries[derived_key] = derived_wrapper
    add_derived_geometry!(prob.equilibrium_wrappers[derived_wrapper.parent_key],
                          derived_key, derived_wrapper.geometry_expr)
    return derived_key
end

function compute_all_targets!(prob::Problem)
    target_values = compute_all_targets(prob)
    foreach(p -> prob.targets[p.first].current_value = p.second, target_values)
end

function compute_all_targets(prob::Problem)
    return compute_targets(prob.targets, prob.equilibrium_wrappers)
end

function compute_targets(targets::Dict{Int, Target},
                         equilibrium_wrappers::Union{EquilibriumWrapper, Dict{UInt, EquilibriumWrapper}};
                        )
    T = typeof(first(targets).second.target_value)
    target_values =Dict{Int, T}()
    wrappers = equilibrium_wrappers isa Dict ? equilibrium_wrappers : Dict(one(UInt) => equilibrium_wrappers)
    for eq_wrapper in values(wrappers)
        for key in keys(eq_wrapper.derived_geometries)
            geometry = extract_derived_geometry(eq_wrapper, key)
            geometry_targets = filter(t -> t.second.geometry_key == key, targets)
            results = compute_targets(deepcopy(geometry_targets), geometry)
            foreach(p -> target_values[p.first] = p.second, results)
        end
    end
    return target_values
end
        

function compute_targets(targets::Dict{Int, Target},
                         geometry::E;
                        ) where {E <: AbstractGeometry}
    T = typeof(first(targets).second.target_value)
    target_values = Dict{Int, T}()
    for (i, target) in targets
        target_values[i] = compute_target(target, geometry)
    end
    return target_values
end

function compute_target(target::Target,
                        geometry::E;
                       ) where {T, E <: AbstractGeometry}
    target_method = Expr(target.method.head, 
                         replace(target.method.args, 
                                 target.geometry_key => geometry)...)
    return eval(target_method)
end

function set_output_directory!(prob::Problem,
                               path::AbstractString;
                              )
    dir = path * (last(path) != '/' ? "/" : "")
    prob.output_directory = dir
    return nothing
end

function Base.clamp(x::T,
                    var::OptimizationVariable{T};
                   ) where {T}
    return clamp(x, var.bounds[1], var.bounds[2])
end

function Base.clamp!(x::AbstractVector{T},
                     state_vars::AbstractVector{OptimizationVariable};
                    ) where {T}
    for i in eachindex(x, state_vars)
        x[i] = clamp(x[i], state_vars[i])
    end
    return nothing
end

