using Base: zero

# The zero function needs to be extended to tuple types
function Base.zero(T::Type{TupleType}) where TupleType <: Tuple
    zeroarr = Array{Any}(undef, length(T.types))
    for (i, t) in enumerate(T.types)
        zeroarr[i] = zero(t)
    end
    return tuple(zeroarr...)
end

"""
    Target{T} <: AbstractOptTarget

Concrete type holding the information to define a physics target.

# Fields:
- `target_value::T`  # Desired target value
- `weight_value::T`  # Desired target weight
- `current_value::T`  # Current value of the target
- `runtime::Float64`  # Most recent time required to compute target value
- `method::Expr`  # An expression to be evaluated on runtime to compute the target function
- `geometry_key::UInt`  # The hash value corresponding to the geometry required to calculation
- `label::String`  # String label for the target, defaults to the method name

See also: [`Problem`](@ref)
"""
mutable struct Target{T} <: AbstractOptTarget
    target_value::T
    target_weight::T
    current_value::T
    gradient::Vector{T}
    method::Expr
    geometry_key::UInt
    label::String
end

Base.show(io::IO, ::MIME"text/plain", t::Target) = print(io, "Target($(t.label), target=$(t.target_value),\tweight=$(t.target_weight),\tcurrent value=$(t.current_value),\tgeometry hash:$(t.geometry_key)\n       method=$(t.method))")


function Target(target,
                weight,
                method::Expr,
                hash::UInt,
                label::String;
               )
    t2, w2 = promote(target, weight)
    T = typeof(t2)
    return Target{T}(t2, w2, zero(T), Vector{T}(undef, 0), method, hash, label)
end

@inline function name(t::Target)
    String(t.label)
end