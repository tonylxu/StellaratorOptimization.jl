
struct DerivedGeometryWrapper
    geometry_expr::Expr
    parent_key::UInt
end

mutable struct EquilibriumWrapper{T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    eq::E
    input::OrderedDict
    translator::Dict{Int, Symbol}
    derived_geometries::Dict{UInt, Expr}
    grad_method::D
    EquilibriumWrapper{T, E, D}() where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative} = new{T, E, D}(NullEquilibrium(), OrderedDict{Symbol, Any}(), Dict{Int, Symbol}(), Dict{UInt, Expr}(), SPSA())
    EquilibriumWrapper{T, E, D}(a, b, c, d, e) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative} = new{T, E, D}(a, b, c, d, e)
end
  
function EquilibriumWrapper(::Union{E, Type{E}}) where {E <: AbstractMagneticEquilibrium}
    error(ArgumentError("EquilibriumWrapper constructor for $(E) not implemented"))
end

function EquilibriumWrapper(::Union{E, Type{E}}, ::Type{D}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper constructor for $(E) not implemented"))
end

function EquilibriumWrapper(::Union{E, Type{E}}, ::Type{D}, ::Type{F}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative, F <: Function}
    error(ArgumentError("EquilibriumWrapper constructor for $(E) not implemented"))
end

function EquilibriumWrapper(::AbstractDict, ::Type{E}) where {E <: AbstractMagneticEquilibrium}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper(::AbstractDict, ::Type{E}, ::D) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper(::AbstractDict, ::Type{E}, ::Type{D}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper(::AbstractDict, ::Type{E}, ::Type{D}, ::Type{F}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative, F <: Function}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper{T, E, D}(::AbstractDict) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper constructor for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper{T, E, D}(::AbstractDict, ::Type{F}) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative, F <: Function}
    error(ArgumentError("EquilibriumWrapper constructor for $(E) from an input dictionary not yet implemented"))
end

function EquilibriumWrapper(::AbstractString, ::Type{E}) where {E <: AbstractMagneticEquilibrium}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input file not yet implemented"))
end

function EquilibriumWrapper(::AbstractString, ::Type{E}, ::Type{F}) where {E <: AbstractMagneticEquilibrium, F <: Function}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input file not yet implemented"))
end

function EquilibriumWrapper(::AbstractString, ::Type{E}, ::Type{D}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input file not yet implemented"))
end

function EquilibriumWrapper(::AbstractString, ::Type{E}, ::Type{D}, ::Type{F}) where {E <: AbstractMagneticEquilibrium, D <: AbstractDerivative, F <: Function}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input file not yet implemented"))
end

function EquilibriumWrapper{T, E, D}(::AbstractString) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error(ArgumentError("EquilibriumWrapper construction for $(E) from an input file not yet implemented"))
end

function equilibrium_type(::EquilibriumWrapper{T, E, D}) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    return E
end

function derivative_type(::EquilibriumWrapper{T, E, D}) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    return D
end

function equilibrium_type_string(::EquilibriumWrapper{T, E, D}) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    return string(nameof(E))
end

function derivative_type_string(::EquilibriumWrapper{T, E, D}) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    return string(nameof(D))
end

function set_input!(::EquilibriumWrapper{T, E, D}, args...) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("Method for updating wrapper input from wrapper equilibrium for EquilibriumWrapper{$E} not yet implemented")
end

function set_input!(wrapped_eq::EquilibriumWrapper{T, E, D},
                    new_input::OrderedDict;
                   ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    for (key, value) in new_input
        wrapped_eq.input[key] = value
    end
    nothing
end

function update_input!(::EquilibriumWrapper{T, E, D}, args...) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("Method for updating wrapper input from wrapper equilibrium for EquilibriumWrapper{$E} not yet implemented")
end

function update_input!(wrapped_eq::EquilibriumWrapper{T, E, D},
                       index::Int,
                       entry;
                     ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    wrapped_eq.input[wrapped_eq.translator[index]] = entry
    nothing
end

function set_translator!(wrapped_eq::EquilibriumWrapper{T, E, D},
                         new_translator::AbstractDict{Int, Symbol};
                        ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    for (key, value) in new_translator
        wrapped_eq.translator[key] = value
    end
    nothing
end

function update_translator!(wrapped_eq::EquilibriumWrapper{T, E, D},
                            index::Int,
                            key::Symbol;
                           ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    wrapped_eq.translator[index] = key
    nothing
end


function translate!(::EquilibriumWrapper{T, E, D},
                    ::Vector{V};
                   ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative, V <: AbstractOptVariable}
    error("Method for translating state vector into equilibrium input for EquilibriumWrapper{$(E)} not implemented")
end

function compute_equilibrium(::EquilibriumWrapper{T, E, D} where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative})
    error("Method for computing equilbrium for EquilibriumWrapper{$(E)} not implemented")
end

function update_equilibrium!(wrapped_eq::EquilibriumWrapper{T, E, D},
                             new_eq::E;
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    setfield!(wrapped_eq, :eq, new_eq)
    nothing
end

function add_derived_geometry!(wrapped_eq::EquilibriumWrapper{T, E, D},
                               key::UInt,
                               geometry_expr::Expr;
                              ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    wrapped_eq.derived_geometries[key] = geometry_expr
    return nothing
end

function compute_target(::EquilibriumWrapper{T, E, D},
                        ::Expr;
                       ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("`compute_target` function for EquilibriumWrapper{$(E)} not implemented")
end

function extract_derived_geometry(::EquilibriumWrapper{T, E, D},
                                  key::UInt;
                                 ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("extract_derived_geometry function not implemented for EquilibriumWrapper{$E}")
end

function write_input_file(::EquilibriumWrapper{T, E, D},
                          filename::AbstractString;
                         ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}
    error("write_input_file not implemented for EquilibriumWrapper{$E}")
end
