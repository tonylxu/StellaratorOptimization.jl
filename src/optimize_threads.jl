
function optimize_threads(prob::Problem{T}) where {T}
    # Define the callback function that requires knowledge of the problem
    function cb_function(x)
        @debug "Executing the callback at iteration $(prob.iteration)"
        update_history!(x, prob)
    end

    # Extract initial state vector of BitType data
    initial_state_vector = state_vector_values(prob)

    # Set the intial gradient of each target to zero
    for i in 1:length(prob.targets)
        prob.targets[i].gradient = zeros(T, length(prob.state_vector))
    end

    # Augment the arguments of the optimizer options with the callback
    option_args = Dict{Symbol, Any}(:callback => cb_function)
    for f in propertynames(prob.optimizer.options)
        val = getfield(prob.optimizer.options, f)
        if !isnothing(val)
            option_args[f] = val
        end
    end
    if !haskey(option_args, :extended_trace)
        option_args[:extended_trace] = true
    end
    if !haskey(option_args, :store_trace)
        option_args[:store_trace] = true
    end
    new_options = (; option_args...)

    write_history_attributes(prob)
    log_io = open(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_log.jl"), "w+")
    logger = SimpleLogger(log_io, Logging.Info)
    try
        residual = with_logger(logger) do
            Optim.optimize(Optim.only_fg!((F, G, θ) -> objective_gradient!(F, G, θ, prob; logger_io = log_io)),
                           initial_state_vector, prob.optimizer.method, Optim.Options(; new_options...))
        end
        JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_problem.jld2"), prob)
        JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_residual.jld2"), residual)
        flush(log_io)
        close(log_io)
        return residual
    catch
        JLD2.save_object(joinpath(prob.output_directory, "stellopt_$(prob.output_tag)_problem.jld2"), prob)
        flush(log_io)
        close(log_io)
        return nothing
    end

end

function objective_gradient!(F,
                             G,
                             θ::AbstractVector,
                             prob::Problem{T};
                             logger_io::Union{Nothing, IOStream} = nothing,
                            ) where {T}
    @debug "Calling objective_gradient at iteration $(prob.iteration)"
    clamp!(θ, prob.state_vector)
    adjust_state_vector!(prob, θ)
    obj = zero(T)
    for (key, wrapper) in prob.equilibrium_wrappers
        var_indices = findall(v -> v.equilibrium_key == key, prob.state_vector)
        eq_targets = filter(p -> prob.derived_geometries[p.second.geometry_key].parent_key == key, prob.targets)
        grad_vec = !isnothing(G) ? Vector{T}(undef, length(var_indices)) : nothing 
        target_gradients = Dict{Int, Vector{T}}()
        obj_part += objective_gradient!(F, grad_vec, wrapper,
                                   prob.state_vector[var_indices],
                                   eq_targets, target_gradients) 
        if !isnothing(obj_part)
            obj += obj_part
        end
        if !isnothing(G)
            foreach(p -> G[p[2]] = grad_vec[p[1]], enumerate(var_indices))
            for t in keys(eq_targets)
                copyto!(prob.targets[t].gradient, target_gradients[t])
            end
        end
    end
    if !isnothing(F)
        adjust_objective!(prob, obj)
    end
    if !isnothing(G)
        adjust_gradient!(prob, copy(G))
    end
    !isnothing(logger_io) ? flush(logger_io) : nothing
    if !isnothing(F)
        return obj
    end
    nothing
end

function objective_gradient!(F,
                             G,
                             wrapper::EquilibriumWrapper{T, E, D},
                             state_vector::AbstractVector{OptimizationVariable},
                             targets::Dict{Int, Target},
                             target_gradients::Dict{Int, Vector{T}}; 
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: Union{CenteredDiff, ForwardDiff}}
    g = wrapper.grad_method
    obj, grad = (nothing, nothing)
    if !isnothing(F) 
        obj = compute_objective(wrapper, state_vector, targets)
    end
    if !isnothing(G)
        if !isnothing(obj)
            grad, target_derivatives = gradient(wrapper, deepcopy(state_vector), targets)
            copyto!(G, grad)
            for key in keys(target_derivatives)
                target_gradients[key] = copy(target_derivatives[key])
            end
        end
    end
    return !isnothing(obj) ? obj : 1e10
end

function compute_objective(wrapper::EquilibriumWrapper{T, E, D},
                           state_vector::AbstractVector{OptimizationVariable},
                           targets::Dict{Int, Target},
                          ) where {T, E <: AbstractMagneticEquilibrium, D <: AbstractDerivative}

    translate!(wrapper, state_vector)
    eq = compute_equilibrium(wrapper)
    if !isnothing(eq)
        update_equilibrium!(wrapper, eq)
        target_values = compute_targets(targets, wrapper) 
        F = zero(T)
        for key in keys(target_values)
            F += (targets[key].target_value - target_values[key])^2/targets[key].target_weight^2 
            targets[key].current_value = target_values[key]
        end
        return F
    else
        return nothing
    end
end 
