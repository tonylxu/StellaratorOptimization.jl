"""
Implements simultaneous perturbation stochastic approximation method
of J.C. Spall, **IEEE Trans. Automatic Control**, *37*, 3, (1992)

θₖ₊₁ = θₖ - ϵ/(β + k)^α gₖ(θ);
gₖ(θ) = f(θₖ .+ cₖ Δₖ) .- f(θₖ .- cₖ Δₖ) ./ (2 * cₖ Δ_k);
cₖ = δ/k^γ 
"""
mutable struct SPSA{T} <: AbstractDerivative
    k::Int
    α::T
    β::T
    γ::T
    δ::T
    ϵ::T
    n::Int
    ∇f::Vector{T}
    f::T
    targets::Dict{Int, T}
    max_evals::Int
    SPSA{T}(a, b, c, d, e, f, g, h, i, j, k) where{T} = new{T}(a, b, c, d, e, f, g, h, i, j, k)
end

function SPSA(; α = 0.602, β = 100.0, γ = 0.101, δ = 1e-4, ϵ = 0.1, n = 1, max_evals = 10)
    α_p, β_p, γ_p, δ_p, ϵ_p = promote(α, β, γ, δ, ϵ)
    T = typeof(α_p)
    return SPSA{T}(1, α_p, β_p, γ_p, δ_p, ϵ_p, n,
                   Vector{T}(undef, 0), zero(T), Dict{Int, T}(), max_evals)
end

function objective_gradient!(F,
                             G,
                             wrapper::EquilibriumWrapper{T, E, D},
                             state_vector::AbstractVector{OptimizationVariable},
                             targets::Dict{Int, Target},
                             target_gradients::Dict{Int, Vector{T}}; 
                            ) where {T, E <: AbstractMagneticEquilibrium, D <: SPSA}
    g = wrapper.grad_method
    obj, grad = (zero(T), nothing)
    if !isnothing(F) 
        if !iszero(g.f)
            obj = g.f
            for key in keys(g.targets)
                targets[key].current_value = g.targets[key]
            end
        else
            obj = compute_objective(wrapper, state_vector, targets)
            wrapper.grad_method.∇f = zeros(T, length(state_vector))
        end
    end
    if !isnothing(G)
        if !isnothing(obj)
            grad, target_derivatives = gradient(wrapper, deepcopy(state_vector), targets)
            copyto!(G, grad)
            for key in keys(target_derivatives)
                target_gradients[key] = copy(target_derivatives[key])
            end
        end
    end
    return obj
end

"""
    gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, SPSA}, state_variables::Vector{OptimizationVariable}, targets::Dict{Int, Target}) where {T, E}

Routine for calculating the Simultaneous Perturbation Stochastic Approximation of the gradient.  The function also computes the next objective value to take advantage of available work.
"""
function gradient(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                  state_variables::Vector{OptimizationVariable},
                  targets::Dict{Int, Target};
                  kwargs...
                 ) where {T, E <: AbstractMagneticEquilibrium, D <: SPSA}
    g = equilibrium_wrapper.grad_method
    original_eq = deepcopy(equilibrium_wrapper.eq)
    n_samples = g.n
    max_threads = min(n_samples, div(Threads.nthreads(), BLAS.get_num_threads())+1)
    n_threads = max_threads - findfirst(i -> rem(n_samples, i) == 0, max_threads:-1:2) + 1 
    minbatch = div(n_samples, n_threads)

    target_keys = collect(keys(targets))
    #gₖ = fill(zeros(T, length(state_variables)), n_samples)
    gₖ = Vector{Vector{T}}(undef, n_samples)
    xₖ = Vector{Vector{T}}(undef, 2 * n_samples + 2)
    αₖ = Vector{T}(undef, n_samples)
    qₖ = similar(αₖ)
    aₖ = Vector{T}(undef, 2 * n_samples + 2)
    cₖ = g.δ/(g.k^g.γ)

    target_gradients = Vector{Dict{Int, Vector{T}}}(undef, n_samples + 1)
    new_targets = Vector{Dict{Int, T}}(undef, 2 * n_samples + 2)
    new_objective = zeros(T, 2 * n_samples + 2)
    for s in 1:n_samples
        gₖ[s] = zeros(T, length(state_variables))
        target_gradients[s] = Dict{Int, Vector{T}}()
        new_targets[2 * (s - 1) + 1] = Dict{Int, T}()
        new_targets[2 * (s - 1) + 2] = Dict{Int, T}()
    end
    target_gradients[n_samples + 1] = Dict{Int, Vector{T}}()
    new_targets[2 * n_samples + 1] = Dict{Int, T}()
    new_targets[2 * n_samples + 2] = Dict{Int, T}()

    θ = map(x -> x.value, state_variables)
    @batch minbatch=minbatch per=thread for s in 1:n_samples
        forward_targets = Dict{Int, T}()
        backward_targets = Dict{Int, T}()
        eq_forward, eq_backward, Δ = simultaneous_perturbation(equilibrium_wrapper, state_variables, cₖ)
        if !isnothing(eq_forward) && !isnothing(eq_backward)
            update_equilibrium!(equilibrium_wrapper, eq_forward)
            push!(forward_targets, compute_targets(targets, equilibrium_wrapper)...)
            update_equilibrium!(equilibrium_wrapper, eq_backward)
            push!(backward_targets, compute_targets(targets, equilibrium_wrapper)...)
            update_equilibrium!(equilibrium_wrapper, original_eq)
            for t in eachindex(θ)
                state_variables[t].value = θ[t]
            end
            for key in target_keys
                forward_objective = (targets[key].target_value - forward_targets[key])^2/targets[key].target_weight^2
                backward_objective = (targets[key].target_value - backward_targets[key])^2/targets[key].target_weight^2
                target_gradients[s][key] = 0.5 * (forward_targets[key] - backward_targets[key]) ./ Δ
                gₖ[s][:] .+= 0.5 * (forward_objective - backward_objective) ./ Δ
            end
            αₖ[s] = dot(gₖ[s], g.∇f) < zero(T) ? g.α + 1 : g.α
        else
            gₖ[s][:] .= randn() .* g.∇f
            αₖ[s] = g.α
        end
        qₖ[s] = min(one(T), αₖ[s] / g.k + 0.5)
    end

    push!(αₖ, g.α)
    push!(gₖ, g.∇f)
    push!(qₖ, min(one(T), g.α / g.k + 0.5))
    for key in target_keys
        target_gradients[end][key] = targets[key].gradient
    end

    @batch minbatch=2*minbatch per=thread for s in 1:2*n_samples+2
        p = div(s - 1, 2) + 1
        i = mod(s - 1, 2) + 1
        aₖ[s] = ((i - 1) + 0.5) * g.ϵ/((g.β + g.k)^qₖ[p])
        xₖ[s] = θ .- aₖ[s] .* gₖ[p]
        
        for t in eachindex(θ)
            state_variables[t].value = xₖ[s][t]
        end
        translate!(equilibrium_wrapper, state_variables)
        eq = compute_equilibrium(equilibrium_wrapper)
        if !isnothing(eq)
            update_equilibrium!(equilibrium_wrapper, eq)
            push!(new_targets[s], compute_targets(targets, equilibrium_wrapper)...)
            for key in target_keys 
                new_objective[s] += (targets[key].target_value - new_targets[s][key])^2/targets[key].target_weight^2
            end
        else
            update_equilibrium!(equilibrium_wrapper, original_eq)
            new_objective[s] = 2 * g.f
            for key in target_keys
                new_targets[s][key] = 2 * targets[key].current_value
            end
        end
        for t in eachindex(θ)
            state_variables[t].value = θ[t]
        end
        translate!(equilibrium_wrapper, state_variables)
    end
    
    _, min_index = findmin(new_objective)
    min_index2 = div(min_index - 1, 2) + 1
 
    equilibrium_wrapper.grad_method.k = g.k+1 
    equilibrium_wrapper.grad_method.α = αₖ[min_index2]
    equilibrium_wrapper.grad_method.∇f = gₖ[min_index2]
    equilibrium_wrapper.grad_method.f = new_objective[min_index]
    equilibrium_wrapper.grad_method.targets = new_targets[min_index]
    
    @debug "aₖ = $(aₖ[min_index])"
    @debug "aₖ * gₖ = $(gₖ[min_index2])"
    return aₖ[min_index] .* gₖ[min_index2], target_gradients[min_index2]
end
"""
    simultaneous_perturbation(equilibrium_wrapper::EquilibriumWrapper{T, E, SPSA}, state_variables::AbstractVector{OptimizationVariable}, targets::Dict{Int, Target}) where {T, E}

Routine for computing the forward and backward simulatneous perturbation of the equilibrium for the simultaneous perturbation stochastic approximation of the gradient.
"""
function simultaneous_perturbation(equilibrium_wrapper::EquilibriumWrapper{T, E, D},
                                   state_variables::AbstractVector{OptimizationVariable},
                                   cₖ::S;
                                  ) where {S, T, E <: AbstractMagneticEquilibrium, D <: SPSA}
    original_input = deepcopy(equilibrium_wrapper.input)
    eq_f = nothing
    eq_b = nothing
    θ = map(v -> v.value, state_variables)
    δθ = similar(θ)
    Δ = similar(θ)
    eval_count = 0
    while (isnothing(eq_f) || isnothing(eq_b)) && eval_count < equilibrium_wrapper.grad_method.max_evals
        b_dist = Bernoulli()
        Δ .= rand(b_dist, length(state_variables)) |> x -> cₖ * (2x .- 1)
        δθ .= θ .+ Δ
        foreach(p -> state_variables[p[1]].value = p[2], enumerate(δθ))
        translate!(equilibrium_wrapper, state_variables)
        eq_f = compute_equilibrium(equilibrium_wrapper)

        set_input!(equilibrium_wrapper, original_input)
        δθ .= θ .- Δ
        foreach(p -> state_variables[p[1]].value = p[2], enumerate(δθ))
        translate!(equilibrium_wrapper, state_variables)
        eq_b = compute_equilibrium(equilibrium_wrapper)
        
        set_input!(equilibrium_wrapper, original_input)
        eval_count += 1
    end
    return eq_f, eq_b, Δ
end

