# StellaratorOptimization.jl

[![Docs](https://img.shields.io/badge/docs-dev-blue.svg)](https://wistell.gitlab.io/StellaratorOptimization.jl/docs)
[![Build Status](https://gitlab.com/wistell/StellaratorOptimization.jl/badges/master/pipeline.svg)](https://gitlab.com/wistell/StellaratorOptimization.jl/pipelines)
[![Coverage](https://gitlab.com/wistell/StellaratorOptimization.jl/badges/master/coverage.svg)](https://gitlab.com/wistell/StellaratorOptimization.jl/commits/master)

A Julia package for performing flexible, distributed stellarator optimization

## Primary Maintainers
    - Benjamin Faber <bfaber AT wisc DOT edu>
    - Aaron Bader <abader AT engr DOT wisc DOT edu>
