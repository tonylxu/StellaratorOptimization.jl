using StellaratorOptimization
using Documenter

DocMeta.setdocmeta!(StellaratorOptimization, :DocTestSetup, :(using StellaratorOptimization); recursive=true)

makedocs(;
    modules=[StellaratorOptimization],
    authors="Benjamin Faber <bfaber@wisc.edu> and contributors",
    repo="https://gitlab.com/WISTELL/StellaratorOptimization.jl/blob/{commit}{path}#{line}",
    sitename="StellaratorOptimization.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://wistell.gitlab.io/StellaratorOptimization.jl",
        assets=String[],
    ),
    pages=[
        "StellaratorOptimization.jl" => "index.md",
        "Library" => [
                      "Private" => "library/internals.md",
                     ],
    ],
)
