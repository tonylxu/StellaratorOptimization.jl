```@meta
CurrentModule = StellaratorOptimization
```

# StellaratorOptimization.jl

`StellaratorOptimization` is a package for optimizing stellarator equilibria. The code is meant to be written in a general sense, but the examples here will use the [VMEC](https://gitlab.com/wistell/VMEC.jl) implementation.

The goal is to minimize:
```math
F = \sum_i \left[ \frac{f_i \left( \mathbf{R} \right) - t_i}{w_i}\right]^2
```
where ``\mathbf{R}`` represents the coefficients that define the equilibrium, for example the Fourier components of the VMEC boundary. ``t_i`` is the user set target. ``w_i`` is the user set weights. Currently only targeted optimization is available, although constraint based optimization is available.

The functions ``f_i`` represent different choices for the optimization target. These can come from anywhere, including those that are user defined, although in this implementation we limit ourselves to choices from [StellaratorOptimizationMetrics.jl](https://gitlab.com/wistell/StellaratorOptimizationMetrics.jl). 

## Usage

To use `StellaratorOptimization`, use the package manager to download and install:
```
julia> using Pkg;
julia> Pkg.add(url="https://gitlab.com/wistell/StellaratorOptimization.jl.git");
julia> using StellaratorOptimization
```

In this section we'll walk through a simple optimization script to optimize for two quantities (rotational transform and quasiaxisymmetry)

First we load the relevant packages.  The snippet here uses the MPI implementation, which is recommended for optimization problems.

```julia
using MPI, LinearAlgebra, Optim, LineSearches
using VMEC, StellaratorOptimization, StellaratorOptimizationMetrics
```

The next lines set up the MPI commands.
```julia
MPI.Init()
MPI.Barrier(MPI.COMM_WORLD)
procs_per_node = 32
procs_per_eval = 8
comms = Stellopt.setup_mpi_comms(procs_per_eval, procs_per_node)
```

Here the variable `nprocs` are the total number of processors available on each node. `nprocs_per_eval` is the number of processors involved in each iteration. In the above example, 32 processors are available on each node, and at any given point 8 of them will be used to solve the equilibrium and the various target functions

Now we define the optimizer to use. We use the `GradientDescent` optimizer with `BackTracking` from the `Optim.jl` package.
```julia
opt = StellOpt.Optimizer(GradientDescent(alphaguess=1e-7, linesearch=BackTracking()),
                         Optim.Options(iteration=20, f_tol = 1e-8,
                                       allow_f_increases=true))
```
The parameter `alphaguess` ``= \alpha`` represents how large a step to take for the variable parameters. In the sample problem these are changes to the boundary Fourier coefficients in the VMEC equilibrium. The coefficients will each be perturbed by ``\frac{\partial F}{R_j} * \alpha``. If VMEC equilibria fail to converge it may be necessary to reduce `alphaguess`. Alternatively, a value of `alphaguess` that is too low, may prevent meaningful steps from taking place. It may take a few attempts to determine a good value. 

We initialize the problem and set some user specific values for where data should be written, and how it should be formatted
```julia
prob = StellOpt.Problem()
prob.output_tag = "test"
prob.output_directory = "/home/yourname/yourpath/"
Stellopt.add_optimizer!(prob, opt)
```

Next we initialize the VMEC equilibrium. We do this by reading a standard vmec input file. Note, there can be formatting problems, sometimes VMEC will read a file that Julia cannot, and vice versa. For this case we use a circular tokamak example file that has been included (somewhere)
```julia
nml = VMEC.read_vmec_namelist("yourpath/input.circular_tokamak")
```

We now finish setting up the problem by making the vmec wrapper and adding it to the problem. We also specify the derivative option here. Since VMEC does not calculate derivatives inherently, we are forced to use a finite differencing scheme. 
```julia
vmec_wrapper = StellOpt.EquilibriumWrapper(nml, Vmec, StellOpt.ForwardDiff(1e-4))
StellOpt.add_equilibrium_wrapper!(prob, vmec_wrapper)
```

Next we set up the targets. In this case we will optimize for two targets. The rotational transform at the ``s = 0.9`` and the value of quasiaxisymmetry on the ``s = 0.6`` surface. The first two lines of code define the two surfaces that we need. The second two lines add the targets to the problem.
```julia
vmec_s06 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.6, equilibrium = vmec_wrapper))
vmec_s09 = StellOpt.@add_derived_geometry!(prob, VmecSurface(0.6, equilibrium = vmec_wrapper))
#quasisymmetry
qs_target = 0.0
qs_weight = 1e-2
qs_label = "qs(s = 0.6)"
StellOpt.@add_target!(prob, VMEC.quasisymmetry_deviation(1, 0, 16, 16, vmec_s06),
                      qs_target, qs_weight, qs_label)
#rotational transform
iota_target = 0.52
iota_weight = 1e-1
iota_label = "iota(s = 0.9)"
StellOpt.@add_target!(prob, VMEC.rotational_transform(vmec_s09), 
                      iota_target, iota_weight, iota_label)
```

Finally we add the Fourier modes to vary. In this case we will use a shortcut to add all ``m, n`` modes below 2. 
```julia
StellOpt.add_multiple_modes!(prob, vmec_wrapper, -2, 2)
```

After the setup is done we can call the optimizer and output the residual.
```julia
residual = StellOpt.optimize_mpi(prob, comms)
```

## Post-Processing

The main results from an optimization run are a series of vmec input files for each optimization step, and a history file which will look like `stellopt_history_<run_name>.h5` format.  There are a few tools that are useful to read the h5 history file. As an example in this section we provide a script to read in a history file and output a plot of the weighted value for each target as a function of iteration.

```julia
data = StellaratorOptimization.read_history_h5("path_to_your_stellopt_history_file")
```

This file loads in the h5 file and returns a Julia struct

```julia
niter = length(data.objective) # number of iterations
valarr = (data.values .- data.targets).^2 ./ data.weights.^2
```

The array here is the individual components in the sum of ``F`` above. It is two dimensional with the first dimension being the different targets and the second dimensionbeing the iterations. At this point you can use any of your preferred programs to plot. The lines below show how to plot it using `Plots.jl`

```julia
using Plots
iter_arr = similar(valarr)
for i in 1:niter
  iter_arr[i,:] .= i
end
plot(iter_arr, valarr, label=reshape(data.target_ids, 1, length(data.target_ids)), ylims=(1E-3, Inf), yaxis=:log, legend=:bottomleft)
```
